### Prerequisites

* Terraform 13.0 or greater 
* A aws account. 
* AWS credentials where terraform can find them. The default location is $HOME/.aws/credentials

### To run

* Initalize terraform:
    * `terraform init`

* Run the terraform plan
    * `terraform plan`

* Execute the terraform plan
    * `terraform apply`
    * At the prompt `Do you want to perform these actions?` Enter `yes` to execute

You can then grab the public DNS of the instance from the EC2 console 
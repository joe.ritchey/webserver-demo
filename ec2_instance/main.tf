provider "aws" {
  region = "us-east-2"
}

locals {
  user_data = <<EOF
#!/bin/bash
sudo apt update
sudo apt install -y openssl nginx
sudo rm -rf /etc/nginx/sites-enabled/default
sudo rm -rf /var/www/html/index.nginx-debian.html
echo 'Hello World!' >> /var/www/html/index.html
sudo echo 'server {
  listen 80;
  listen [::]:80;
  index index.html index.htm index.nginx-debian.html;
  server_name _;
	return 301 https://$host$request_uri;
}

server {
  listen 443 ssl http2;
  server_name demo.netadmin.co;
  root /var/www/html;
  ssl_certificate /etc/ssl/certs/demo.netadmin.co.crt;
  ssl_certificate_key /etc/ssl/private/demo.netadmin.co.key;
  index index.html index.htm index.nginx-debian.html;
}
' > /etc/nginx/sites-enabled/netadmin
openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 -subj "/C=US/ST=VA/L=Fairfax/O=Dis/CN=demo.netadmin.co" \
    -keyout /etc/ssl/private/demo.netadmin.co.key -out /etc/ssl/certs/demo.netadmin.co.crt
sudo service nginx restart
EOF
}

data "aws_vpc" "default" {
  default = true
}

data "aws_subnet_ids" "all" {
  vpc_id = data.aws_vpc.default.id
}

data "aws_ami" "ubuntu" {

    most_recent = true

    filter {
        name   = "name"
        values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
    }

    filter {
        name = "virtualization-type"
        values = ["hvm"]
    }

    owners = ["099720109477"]
}

module "security_group" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 3.0"

  name        = "http-https"
  description = "Security group for example usage with EC2 instance"
  vpc_id      = data.aws_vpc.default.id

  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["http-80-tcp", "https-443-tcp", "all-icmp"]
  egress_rules        = ["all-all"]
}

resource "aws_eip" "this" {
  vpc      = true
  instance = module.ec2.id[0]
}

resource "aws_network_interface" "this" {
  count = 1

  subnet_id = tolist(data.aws_subnet_ids.all.ids)[count.index]
}

module "ec2" {
  source  = "terraform-aws-modules/ec2-instance/aws"

  instance_count = 1

  name          = "demo-ssl"
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t3.medium"
  cpu_credits                 = "unlimited"
  subnet_id     = tolist(data.aws_subnet_ids.all.ids)[0]
  vpc_security_group_ids      = [module.security_group.this_security_group_id]
  associate_public_ip_address = true
 
  user_data_base64 = base64encode(local.user_data)
  tags = {
    "env"                 = "sandbox"
    "cost-center"         = "retail"
  }
}

